Source: python-doubleratchet
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
	dh-python,
	python3-all,
	python3-cryptography,
	python3-pydantic (>= 2~),
	python3-pytest,
	python3-pytest-asyncio,
	python3-setuptools,
Standards-Version: 4.2.1
Rules-Requires-Root: no
Homepage: https://github.com/Syndace/python-doubleratchet
Vcs-Git: https://salsa.debian.org/python-team/packages/python-doubleratchet.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-doubleratchet
Testsuite: autopkgtest-pkg-pybuild

Package: python3-doubleratchet
Architecture: all
Depends: ${misc:Depends},
	${python3:Depends},
Description: Python 3 implementation of the Double Ratchet algorithm
 This python library offers an implementation of the Double Ratchet
 algorithm as specified here.
 .
 The goal is to provide a configurable and independent implementation of
 the algorithm, while keeping the structure close to the specification
 and providing recommended settings.
 .
 This library was developed as part of python-omemo, a pretty cool
 end-to-end encryption protocol.
 .
 This package provides the Python 3.x module.
